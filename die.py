from random import randint


class Die:
    def __init__(self, sides=6):
        self.sides = sides
        self.face_up = 0
        self.unlocked = True

    def roll(self):
        if self.unlocked:
            self.face_up = randint(1, self.sides)

    def lock(self):
        self.unlocked = False

    def unlock(self):
        self.unlocked = True
