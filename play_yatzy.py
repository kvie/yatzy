import curses
from game import Game


def main(screen):
    # Play the Yatzy Game!
    curses.use_default_colors()
    curses.echo()
    curses.nocbreak()
    screen.keypad(False)

    game = Game(screen)
    game.greet()


curses.wrapper(main)
