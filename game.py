from hand import Hand
from scorecard import Scorecard


class Game:
    def __init__(self, screen):
        self.screen = screen
        self.hand = Hand()
        self.scorecard = Scorecard()

    def greet(self):
        self.screen.clear()
        self.screen.addstr(0, 0, "Welcome to Yatzy!")
        self.screen.addstr(1, 0, "Type 'help' to view the instructions,")
        self.screen.addstr(2, 0, "or 'quit' at any time to end the game.")
        self.screen.addstr(4, 0, "Press ENTER to continue. ")

        command = self.screen.getstr().decode()

        if command:
            if command.lower()[0] == 'q':
                quit()
            elif command.lower()[0] == 'h':
                self.instructions()

        self.play()

    def display_gameboard(self):
        self.screen.clear()

        self.display_scorecard()
        self.display_hand()
        self.display_possible_scores()

    def display_scorecard(self):
        scorecard_view = self.screen.subwin(23, 28, 0, 0)
        scorecard_view.immedok(True)

        scorecard_view.addstr(0, 0, self.get_scorecard_str())

    def get_scorecard_str(self):

        score_str = "SCORECARD:\n"
        score_str += "==========================\n"

        score_str += "| Ones             | "
        score_str += str(self.scorecard.scores['ones'] or ' ').rjust(3)
        score_str += " |\n"
        score_str += "| Twos             | "
        score_str += str(self.scorecard.scores['twos'] or ' ').rjust(3)
        score_str += " |\n"
        score_str += "| Threes           | "
        score_str += str(self.scorecard.scores['threes'] or ' ').rjust(3)
        score_str += " |\n"
        score_str += "| Fours            | "
        score_str += str(self.scorecard.scores['fours'] or ' ').rjust(3)
        score_str += " |\n"
        score_str += "| Fives            | "
        score_str += str(self.scorecard.scores['fives'] or ' ').rjust(3)
        score_str += " |\n"
        score_str += "| Sixes            | "
        score_str += str(self.scorecard.scores['sixes'] or ' ').rjust(3)
        score_str += " |\n"

        score_str += "--------------------------\n"

        score_str += "| Bonus?           | "
        score_str += str(self.scorecard.scores['bonus'] or ' ').rjust(3)
        score_str += " |\n"

        score_str += "--------------------------\n"

        score_str += "| One Pair         | "
        score_str += str(self.scorecard.scores['one pair'] or ' ').rjust(3)
        score_str += " |\n"
        score_str += "| Two Pairs        | "
        score_str += str(self.scorecard.scores['two pairs'] or ' ').rjust(3)
        score_str += " |\n"
        score_str += "| Three of a Kind  | "
        score_str += str(self.scorecard.scores['three of a kind']
                         or ' ').rjust(3)
        score_str += " |\n"
        score_str += "| Four of a Kind   | "
        score_str += str(self.scorecard.scores['four of a kind']
                         or ' ').rjust(3)
        score_str += " |\n"
        score_str += "| Small Straight   | "
        score_str += str(self.scorecard.scores['small straight']
                         or ' ').rjust(3)
        score_str += " |\n"
        score_str += "| Large Straight   | "
        score_str += str(self.scorecard.scores['large straight']
                         or ' ').rjust(3)
        score_str += " |\n"
        score_str += "| Full House       | "
        score_str += str(self.scorecard.scores['full house'] or ' ').rjust(3)
        score_str += " |\n"
        score_str += "| Chance           | "
        score_str += str(self.scorecard.scores['chance'] or ' ').rjust(3)
        score_str += " |\n"
        score_str += "| Yatzy!           | "
        score_str += str(self.scorecard.scores['yatzy'] or ' ').rjust(3)
        score_str += " |\n"

        score_str += "--------------------------\n"
        score_str += "| Total            | "
        score_str += str(self.scorecard.scores['total'] or ' ').rjust(3)
        score_str += " |\n"

        score_str += "=========================="

        return score_str

    def display_hand(self):
        hand_view = self.screen.subwin(6, 22, 0, 30)
        hand_view.immedok(True)

        hand_view.addstr(0, 0, self.get_hand_str())

    def get_hand_str(self):
        hand_str = "HAND:\n"
        hand_str += "=====================\n"
        hand_str += "| A | B | C | D | E |\n"
        hand_str += "---------------------\n"
        hand_str += "|"
        for face in self.hand.dice_faces():
            hand_str += f" {face or ' '} |"
        hand_str += "\n====================="
        return hand_str

    def display_possible_scores(self):
        p_score_view = self.screen.subwin(20, 25, 7, 30)
        p_score_view.immedok(True)

        p_score_view.addstr(0, 0, self.get_p_score_str())

    def get_p_score_str(self):
        p_score_str = "POSSIBLE SCORES:\n"
        for name, score in self.scorecard.possible_scores(self.hand).items():
            p_score_str += f"{name.title()} ({score})\n"
        return p_score_str

    def play_round(self):
        self.hand.roll_dice()
        rerolls_left = 2
        self.display_gameboard()
        prompt_view = self.screen.subwin(10, 70, 24, 0)
        keep_msg = "Enter the letters of the dice you would like to keep. "
        keep_msg += "('A B C')\n"
        keep_msg += f"You have {rerolls_left} re-rolls left. Type 'STOP'"
        keep_msg += " to move on to scoring.\n\n"
        keep_msg += "    > "

        commands = False
        while not commands:
            prompt_view.addstr(0, 0, keep_msg)
            command = prompt_view.getstr().decode()
            if command:
                if command.lower()[0] == 'q':
                    quit()
                elif command.lower()[0] == 'h':
                    self.instructions()
                elif command.lower()[0] == 's':
                    rerolls_left = 0
                else:
                    commands = self.validate_command(command)

    def validate_command(self, commands):
        # commands = full_command.split()
        for char in commands:
            if char.lower() not in ['a', 'b', 'c', 'd', 'e', ' ']:
                self.screen.addstr(30, 0, "INVALID COMMAND")
                return False
        return commands

    def play(self):
        self.play_round()
        self.screen.getstr()

    def instructions(self):
        pass
