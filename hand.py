from die import Die


class Hand:
    def __init__(self, num_dice=5):
        self.dice = [Die() for i in range(num_dice)]
        self.score_matches = []

    def lock_dice(self, dice_indices):
        for dice_index in dice_indices:
            self.dice[dice_index].lock()

    def roll_dice(self):
        for die in self.dice:
            die.roll()
        self.compatible_scores()  # update on every roll

    def unlock_hand(self):
        for die in self.dice:
            die.unlock()

    def dice_faces(self):
        return [die.face_up for die in self.dice]

    def compatible_scores(self):
        self.score_matches = ["chance"]
        self.loose_nums(1, "ones")
        self.loose_nums(2, "twos")
        self.loose_nums(3, "threes")
        self.loose_nums(4, "fours")
        self.loose_nums(5, "fives")
        self.loose_nums(6, "sixes")
        self.one_pair()
        self.two_pairs()
        self.three_kind()
        self.four_kind()
        self.small_straight()
        self.large_straight()
        self.full_house()
        self.yatzy()

    def loose_nums(self, num, score_name):
        if num in self.dice_faces():
            self.score_matches.append(score_name)

    def one_pair(self):
        faces = self.dice_faces()
        for face in set(faces):
            if faces.count(face) >= 2:
                self.score_matches.append("one pair")
                return

    def two_pairs(self):
        faces = self.dice_faces()
        pair_count = 0
        for face in set(faces):
            if faces.count(face) >= 2:
                pair_count += 1
            if pair_count == 2:
                self.score_matches.append("two pairs")
                return

    def three_kind(self):
        faces = self.dice_faces()
        for face in set(faces):
            if faces.count(face) >= 3:
                self.score_matches.append("three of a kind")
                return

    def four_kind(self):
        faces = self.dice_faces()
        for face in set(faces):
            if faces.count(face) >= 4:
                self.score_matches.append("four of a kind")
                return

    def small_straight(self):
        faces = self.dice_faces()
        for num in [1, 2, 3, 4, 5]:
            if num not in faces:
                return
        self.score_matches.append("small straight")

    def large_straight(self):
        faces = self.dice_faces()
        for num in [2, 3, 4, 5, 6]:
            if num not in faces:
                return
        self.score_matches.append("large straight")

    def full_house(self):
        faces = self.dice_faces()
        two = False
        three = False
        for face in set(faces):
            if faces.count(face) == 2:
                two = True
            elif faces.count(face) == 3:
                three = True
            if two and three:
                self.score_matches.append("full house")
                return

    def yatzy(self):
        if len(set(self.dice_faces())) == 1:
            self.score_matches.append("yatzy")
