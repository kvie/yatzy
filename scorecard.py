class Scorecard:
    def __init__(self):
        self.scores = {
            "ones": None,
            "twos": None,
            "threes": None,
            "fours": None,
            "fives": None,
            "sixes": None,
            "bonus": None,
            "one pair": None,
            "two pairs": None,
            "three of a kind": None,
            "four of a kind": None,
            "small straight": None,
            "large straight": None,
            "full house": None,
            "chance": None,
            "yatzy": None,
            "total": None,
        }

    def possible_scores(self, hand):
        # for each possible score
        # if it's in the hand and also an empty score
        # calculate the number for that particular score
        # and add the score name and number to the hash
        p_scores = {}
        for key in self.scores.keys():
            if key in hand.score_matches and self.scores[key] is None:
                p_scores[key] = self.calculate(key, hand.dice_faces())
        return p_scores

    def calculate(self, score_name, dice_faces):
        if score_name == "ones":
            return dice_faces.count(1)
        elif score_name == "twos":
            return dice_faces.count(2) * 2
        elif score_name == "threes":
            return dice_faces.count(3) * 3
        elif score_name == "fours":
            return dice_faces.count(4) * 4
        elif score_name == "fives":
            return dice_faces.count(5) * 5
        elif score_name == "sixes":
            return dice_faces.count(6) * 6
        elif score_name == "one pair":
            # if there's two pairs, return score of highest pair
            pair_nums = [face for face in set(dice_faces)
                         if dice_faces.count(face) >= 2]
            return max(pair_nums) * 2
        elif score_name == "two pairs":
            pair_nums = [face for face in set(dice_faces)
                         if dice_faces.count(face) >= 2]
            return sum(pair_nums) * 2
        elif score_name == "three of a kind":
            for face in set(dice_faces):
                if dice_faces.count(face) >= 3:
                    return face * 3
        elif score_name == "four of a kind":
            for face in set(dice_faces):
                if dice_faces.count(face) >= 4:
                    return face * 4
        elif score_name == "small straight":
            return 15
        elif score_name == "large straight":
            return 20
        elif score_name == "full house" or score_name == "chance":
            return sum(dice_faces)
        elif score_name == "yatzy":
            return 50
